</head>
<body>

<h1>CPE/EEE 185</h1>
<title>Introduction</title>

<p>WPSIO (Which Parking Spot is Open?) is a simple robot that keeps track of the number and location of parking spots available to users. 
This project could be implemented in various different ways, but to keep it simple due to time limit and budget, we are using a robot to the job. 
The robot or WPSIO will have six main functions:<p>

<p> <i>It will move around the parking lot.</i> <p>
<p> <i>It check one side of the parking lot for vehicles parked.</i> <p>
<p> <i>If a vehicle is detected it will take a picture of License Plates and mark it as spot reserved. </i> <p>
<p> <i>If a vehicle is not detected it will mark the spot as available.</i> <p>
<p> <i>If there is vehicle or object, but License Plates are not detected it will mark the spot as reserved by unknown object. </i> <p>
<p> <i>The result will then be transferred into a database in which will later be displayed in a webserver.</i> <p>

<a href=”http://192.168.43.160/data.php”><h2>Data</h2></a>

</body>
</html>